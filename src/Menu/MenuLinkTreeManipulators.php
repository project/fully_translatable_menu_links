<?php

namespace Drupal\fully_translatable_menu_links\Menu;

use Drupal\Core\Menu\MenuLinkBase;

/**
 * Provides menu link tree manipulators.
 */
class MenuLinkTreeManipulators {

  /**
   * Remove disabled links from the menu tree.
   *
   * @param array $tree
   *   Something.
   */
  public function removeDisabledLinks(array $tree) {
    foreach ($tree as $key => $element) {
      if ($element->link instanceof MenuLinkBase) {
        $link = $element->link;
        if (!$link->isEnabled()) {
          unset($tree[$key]);
        }
      }
    }

    return $tree;
  }

}
