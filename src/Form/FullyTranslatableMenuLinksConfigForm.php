<?php

namespace Drupal\fully_translatable_menu_links\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class FullyTranslatableMenuConfigForm.
 */
class FullyTranslatableMenuLinksConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'fully_translatable_menu_links.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fully_translatable_menu_links_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('fully_translatable_menu_links.settings');
    $form['multilingual'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Choose the properties to be made translatable'),
      '#options' => [
        'weight' => $this->t('Weight'),
        'enabled' => $this->t('Enabled'),
      ],
      '#default_value' => $config->get('multilingual'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('fully_translatable_menu_links.settings')
      ->set('multilingual', $form_state->getValue('multilingual'))
      ->save();

    drupal_flush_all_caches();
  }

}
