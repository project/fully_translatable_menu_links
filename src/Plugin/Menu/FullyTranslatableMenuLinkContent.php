<?php

namespace Drupal\fully_translatable_menu_links\Plugin\Menu;

use Drupal\menu_link_content\Plugin\Menu\MenuLinkContent;

/**
 * Provides the menu link plugin for content menu links.
 */
class FullyTranslatableMenuLinkContent extends MenuLinkContent {

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    // Get the value from entity.
    if ($this->languageManager->isMultilingual()) {
      return $this->getEntity()->isEnabled();
    }
    // Fallback to plugin definition.
    return (bool) $this->pluginDefinition['enabled'];
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    // Get the value from entity.
    if ($this->languageManager->isMultilingual()) {
      return $this->getEntity()->getWeight();
    }

    // Or set default the weight to 0.
    if (!isset($this->pluginDefinition['weight'])) {
      $this->pluginDefinition['weight'] = 0;
    }
    // Fallback to plugin definition.
    return $this->pluginDefinition['weight'];
  }

}
